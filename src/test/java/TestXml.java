import com.grd.cpay.base.utils.JaxbUtils;
import com.grd.cpay.payment.wechat.utils.WechatUtils;
import com.grd.cpay.payment.wechat.vo.req.UnifiedOrderReq;
import org.junit.Test;

import java.math.BigDecimal;

public class TestXml {

    private static final String FILE_NAME = "jaxb-orderApiReq.xml";

    @Test
    public void Test1() throws IllegalAccessException {
        UnifiedOrderReq orderApiReq = new UnifiedOrderReq();
        orderApiReq.setAppid("wxd678efh567hg6787");
        orderApiReq.setMchId("1230000109");
        orderApiReq.setDeviceInfo("013467007045764");
        orderApiReq.setNonceStr("5K8264ILTKCH16CQ2502SI8ZNMTM67VS");
        orderApiReq.setSign("C380BEC2BFD727A4B6845133519F3AD6");
        orderApiReq.setBody("腾讯充值中心-QQ会员充值");
        orderApiReq.setDetail("detail");
        orderApiReq.setAttach("深圳分店");
        orderApiReq.setOutTradeNo("20150806125346");
        orderApiReq.setFeeType("CNY");
        orderApiReq.setTotalFee(BigDecimal.valueOf(88));
        orderApiReq.setSpbillCreateIp("123.12.12.123");
        orderApiReq.setTimeStart("20091225091010");
        orderApiReq.setTimeExpire("20091227091010");
        orderApiReq.setGoodsTag("WXG");
        orderApiReq.setNotifyUrl("http://www.weixin.qq.com/wxpay/pay.php");
        orderApiReq.setTradeType("JSAPI");
        orderApiReq.setProductId("12235413214070356458058");
        orderApiReq.setLimitPay("no_credit");
        orderApiReq.setOpenid("oUpF8uMuAJO_M2pxb1Q9zNjWeS6o");
        orderApiReq.setReceipt("Y");
        orderApiReq.setSceneInfo("sceneInfo");


        String s = JaxbUtils.jaxbObjectToXmlString(orderApiReq, UnifiedOrderReq.class);
        System.out.println(s);
        UnifiedOrderReq ord = JaxbUtils.jaxbXmlStringToObject(s, UnifiedOrderReq.class);
        System.out.println(ord.getAppid());

        System.out.println(WechatUtils.getKeyAndValueString(orderApiReq));

    }
}
