package com.grd.cpay.payment.wechat.service;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import com.grd.cpay.base.service.BaseHttpService;
import com.grd.cpay.base.utils.JaxbUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;

public class WechatBaseHttpService extends BaseHttpService {

    private static final String TAG = "WechatBaseHttpService";

    @Value("${wechat.url}")
    private String WECHAT_URL;

    /**
     *
     * @param uri
     * @param httpMethod
     * @param body
     * @param reqClazz
     * @param resClazz
     * @param <T1>
     * @param <T2>
     * @return
     */
    protected <T1, T2> T2 sendHttpRequest(String uri, HttpMethod httpMethod, T1 body, Class<T1> reqClazz, Class<T2> resClazz) {
        String reqBody = JaxbUtils.jaxbObjectToXmlString(body, reqClazz);
        String resBody = super.sendHttpRequest(WECHAT_URL+uri, httpMethod, reqBody);
        if(StringUtils.isNotEmpty(resBody)) {
            return JaxbUtils.jaxbXmlStringToObject(resBody, resClazz);
        } else {
            return null;
        }
    }
}
