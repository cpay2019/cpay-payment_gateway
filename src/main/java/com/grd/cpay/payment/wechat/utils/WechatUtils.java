package com.grd.cpay.payment.wechat.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class WechatUtils {

    /*
            第一步，设所有发送或者接收到的数据为集合M，将集合M内非空参数值的参数按照参数名ASCII码从小到大排序（字典序），使用URL键值对的格式（即key1=value1&key2=value2…）拼接成字符串stringA。
        特别注意以下重要规则：

        ◆ 参数名ASCII码从小到大排序（字典序）；
        ◆ 如果参数的值为空不参与签名；
        ◆ 参数名区分大小写；
        ◆ 验证调用返回或微信主动通知签名时，传送的sign参数不参与签名，将生成的签名与该sign值作校验。
        ◆ 微信接口可能增加字段，验证签名时必须支持增加的扩展字段
     */
    public static <T> String getKeyAndValueString (T t) {

        try {
            Class<? extends Object> c1 = t.getClass();
            Field[] fields = c1.getDeclaredFields();
            List<String> names = new ArrayList<>();
            HashMap<String, Object> map = new HashMap<>();
            for (int i = 0; i < fields.length; i++) {
                String name = fields[i].getName();
                fields[i].setAccessible(true);
                Object value = null;
                value = fields[i].get(t);
                names.add(name);
                map.put(name, value);
            }
            Collections.sort(names);
            StringBuilder sb = new StringBuilder();
            names.stream().forEach(name->{
                sb.append(name).append("=").append(map.get(name)).append("&");
            });
            String s = sb.toString();
            if(s.length()>0) {
                s = s.substring(0, s.length()-1);
            }
            return s;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return new String();
    }

    /*第一步：对参数按照key=value的格式，并按照参数名ASCII字典序排序：
            第二步：拼接API密钥：
        */
    public static String md5digest(String content, String apiKey) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(content + "&key=" +apiKey);
            oos.close();
            MessageDigest e = MessageDigest.getInstance("MD5");
            e.update(baos.toByteArray());
            return new BigInteger(1, e.digest()).toString(16);
        } catch (Exception e) {
            return "";
        }
    }

    public static String genNonceStr() {
        return RandomStringUtils.random(12, true, false);
    }
}
