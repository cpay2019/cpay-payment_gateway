package com.grd.cpay.payment.wechat.vo.res;

import com.grd.cpay.payment.wechat.vo.WechatBaseRes;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "xml")
public class CloseOrderRes extends WechatBaseRes {

    private String resultMsg;

    @XmlElement(name = "result_msg")
    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }


}
