package com.grd.cpay.payment.wechat.service.impl;

import com.grd.cpay.payment.wechat.service.WechatBaseHttpService;
import com.grd.cpay.payment.wechat.service.WechatService;
import com.grd.cpay.payment.wechat.utils.WechatUtils;
import com.grd.cpay.payment.wechat.vo.req.*;
import com.grd.cpay.payment.wechat.vo.res.CloseOrderRes;
import com.grd.cpay.payment.wechat.vo.res.OrderQueryRes;
import com.grd.cpay.payment.wechat.vo.res.UnifiedOrderRes;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class WechatServiceImpl extends WechatBaseHttpService implements WechatService {

    private final String TAG = "WechatService";

    private static final String URI_UNIFIED_ORDER = "/pay/unifiedorder";
    private static final String URI_ORDER_QUERY = "/pay/orderquery";
    private static final String URI_CLOSE_ORDER = "/pay/closeorder";
    private static final String URI_DOWNLOAD_BILL = "/pay/downloadbill";
    private static final String URI_DOWNLOAD_FUND_FLOW = "/pay/downloadfundflow";

    @Override
    public UnifiedOrderRes unifiedOrder(UnifiedOrderReq unifiedOrderReq, String apiKey) {
        unifiedOrderReq.setSign(WechatUtils.md5digest(WechatUtils.getKeyAndValueString(unifiedOrderReq),apiKey));
        return sendHttpRequest(URI_UNIFIED_ORDER, HttpMethod.POST, unifiedOrderReq, UnifiedOrderReq.class, UnifiedOrderRes.class);
    }

    @Override
    public OrderQueryRes orderquery(OrderQueryReq orderQueryReq, String apiKey) {
        orderQueryReq.setSign(WechatUtils.md5digest(WechatUtils.getKeyAndValueString(orderQueryReq),apiKey));
        return sendHttpRequest(URI_ORDER_QUERY, HttpMethod.POST, orderQueryReq, OrderQueryReq.class, OrderQueryRes.class);
    }

    @Override
    public CloseOrderRes closeorder(CloseOrderReq closeOrderReq, String apiKey) {
        closeOrderReq.setSign(WechatUtils.md5digest(WechatUtils.getKeyAndValueString(closeOrderReq),apiKey));
        CloseOrderRes ret = sendHttpRequest(URI_CLOSE_ORDER, HttpMethod.POST, closeOrderReq, CloseOrderReq.class, CloseOrderRes.class);
        return sendHttpRequest(URI_CLOSE_ORDER, HttpMethod.POST, closeOrderReq, CloseOrderReq.class, CloseOrderRes.class);
    }

    @Override
    public String downloadbill(DownloadBillReq downloadBillReq, String apiKey) {
        downloadBillReq.setSign(WechatUtils.md5digest(WechatUtils.getKeyAndValueString(downloadBillReq),apiKey));
        String ret = sendHttpRequest(URI_DOWNLOAD_BILL, HttpMethod.POST, downloadBillReq, DownloadBillReq.class, String.class);
        return ret;
    }

    @Override
    public String downloadfundflow(DownloadFundFlowReq downloadFundFlowReq, String apiKey) {
        downloadFundFlowReq.setSign(WechatUtils.md5digest(WechatUtils.getKeyAndValueString(downloadFundFlowReq),apiKey));
        String ret = sendHttpRequest(URI_DOWNLOAD_FUND_FLOW, HttpMethod.POST, downloadFundFlowReq, DownloadFundFlowReq.class, String.class);
        return ret;
    }
}
