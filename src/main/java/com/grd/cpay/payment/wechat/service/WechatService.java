package com.grd.cpay.payment.wechat.service;

import com.grd.cpay.payment.wechat.vo.req.*;
import com.grd.cpay.payment.wechat.vo.res.CloseOrderRes;
import com.grd.cpay.payment.wechat.vo.res.OrderQueryRes;
import com.grd.cpay.payment.wechat.vo.res.UnifiedOrderRes;

public interface WechatService {

    /**
     * 統一下單
     *
     * @param unifiedOrderReq
     * @param  apiKey
     * @return
     */
    UnifiedOrderRes unifiedOrder(UnifiedOrderReq unifiedOrderReq, String apiKey);

    /**
     * 查询订单
     *
     * @param orderQueryReq
     * @param  apiKey
     * @return
     */
    OrderQueryRes orderquery(OrderQueryReq orderQueryReq, String apiKey);

    /**
     * 关闭订单
     *
     * @param closeOrderReq
     * @param  apiKey
     * @return
     */
    CloseOrderRes closeorder(CloseOrderReq closeOrderReq, String apiKey);

    /**
     * 下载对账单
     *
     * @param downloadBillReq
     * @param  apiKey
     * @return
     */
    String downloadbill(DownloadBillReq downloadBillReq, String apiKey);

    /**
     * 下载资金账单
     *
     * @param downloadFundFlowReq
     * @param  apiKey
     * @return
     */
    String downloadfundflow(DownloadFundFlowReq downloadFundFlowReq, String apiKey);

}
