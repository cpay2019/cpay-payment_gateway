package com.grd.cpay.payment.wechat.vo;

public enum WechatResErrors {

    INVALID_REQUEST("INVALID_REQUEST", "参数错误"),
    NOAUTH("NOAUTH", "商户无此接口权限"),
    NOTENOUGH("NOTENOUGH", "余额不足"),
    ORDERPAID("ORDERPAID", "商户订单已支付"),
    ORDERCLOSED("ORDERCLOSED", "订单已关闭"),
    SYSTEMERROR("SYSTEMERROR", "系统错误"),
    APPID_NOT_EXIST("APPID_NOT_EXIST", "APPID不存在"),
    MCHID_NOT_EXIST("MCHID_NOT_EXIST", "MCHID不存在"),
    APPID_MCHID_NOT_MATCH("APPID_MCHID_NOT_MATCH", "appid和mch_id不匹配"),
    LACK_PARAMS("LACK_PARAMS", "缺少参数"),
    OUT_TRADE_NO_USED("OUT_TRADE_NO_USED", "商户订单号重复"),
    SIGNERROR("SIGNERROR", "签名错误"),
    XML_FORMAT_ERROR("XML_FORMAT_ERROR", "XML格式错误"),
    REQUIRE_POST_METHOD("REQUIRE_POST_METHOD", "请使用post方法"),
    POST_DATA_EMPTY("POST_DATA_EMPTY", "post数据为空"),
    NOT_UTF8("NOT_UTF8", "编码格式错误"),
    ORDERNOTEXIST("ORDERNOTEXIST", "此交易订单号不存在"),
    BIZERR_NEED_RETRY("BIZERR_NEED_RETRY", "退款业务流程错误，需要商户触发重试来解决"),
    TRADE_OVERDUE("TRADE_OVERDUE", "订单已经超过退款期限"),
    ERROR("ERROR", "业务错误"),
    USER_ACCOUNT_ABNORMAL("USER_ACCOUNT_ABNORMAL", "退款请求失败"),
    INVALID_REQ_TOO_MUCH("INVALID_REQ_TOO_MUCH", "无效请求过多"),
    INVALID_TRANSACTIONID("INVALID_TRANSACTIONID", "无效transaction_id"),
    PARAM_ERROR("PARAM_ERROR", "参数错误"),
    FREQUENCY_LIMITED("FREQUENCY_LIMITED", "频率限制"),
    REFUNDNOTEXIST("REFUNDNOTEXIST", "退款订单查询失败"),
    INVALID_BILL_TYPE("INVALID_BILL_TYPE", "参数错误"),
    DATA_FORMAT_ERROR("DATA_FORMAT_ERROR", "参数错误"),
    MISSING_PARAMETER("MISSING_PARAMETER", "参数错误"),
    SIGN_ERROR("SIGN_ERROR", "参数错误"),
    NO_BILL_EXIST("NO_BILL_EXIST", "账单不存在"),
    BILL_CREATING("BILL_CREATING", "账单未生成"),
    COMPRESSGZIP_ERROR("COMPRESSGZIP_ERROR", "账单压缩失败"),
    UNCOMPRESSGZIP_ERROR("UNCOMPRESSGZIP_ERROR", "账单解压失败"),
    NO_COMMENT("NO_COMMENT", "对应的时间段没有用户的评论数据"),
    TIME_EXPIRE("TIME_EXPIRE", "拉取的时间超过3个月");

    private String errCode;
    private String errCodeDes;

    WechatResErrors(String errCode, String errCodeDes) {
        this.errCode = errCode;
        this.errCodeDes = errCodeDes;
    }

    public String getErrCode() {
        return errCode;
    }

    public String getErrCodeDes() {
        return errCodeDes;
    }
}
