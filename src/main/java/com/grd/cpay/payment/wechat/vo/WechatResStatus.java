package com.grd.cpay.payment.wechat.vo;

public enum WechatResStatus {

    SUCCESS("SUCCESS", "OK"),
    FAIL("FAIL", "FAIL");

    private String returnCode;
    private String returnMsg;

    WechatResStatus(String returnCode, String returnMsg) {
        this.returnCode = returnCode;
        this.returnMsg = returnMsg;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }
}
