package com.grd.cpay.payment.gateway.vo.req;

import io.swagger.annotations.ApiModelProperty;
import com.grd.cpay.base.enums.ChannelType;

public class BillReq {

    @ApiModelProperty(value="支付渠道")
    private ChannelType channelType;

    @ApiModelProperty(value="API Key")
    private String apiKey;

    @ApiModelProperty(value="公众账号ID")
    private String appId;

    @ApiModelProperty(value="商户号")
    private String mchId;

    @ApiModelProperty(value="对账单日期", example = "20181225")
    private String billDate;

    @ApiModelProperty(value="账单类型")
    private String billType;

    @ApiModelProperty(value="压缩账单")
    private String tarType;

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getTarType() {
        return tarType;
    }

    public void setTarType(String tarType) {
        this.tarType = tarType;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
