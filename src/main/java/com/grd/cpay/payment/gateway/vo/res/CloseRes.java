package com.grd.cpay.payment.gateway.vo.res;

import io.swagger.annotations.ApiModelProperty;

public class CloseRes {

    @ApiModelProperty(value="公众账号ID")
    private String appId;

    @ApiModelProperty(value="商户号")
    private String mchId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

}
