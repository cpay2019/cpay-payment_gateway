package com.grd.cpay.payment.gateway.vo.req;

import io.swagger.annotations.ApiModelProperty;
import com.grd.cpay.base.enums.ChannelType;

public class CancelReq {

    @ApiModelProperty(value="支付渠道")
    private ChannelType channelType;

    @ApiModelProperty(value="API Key")
    private String apiKey;

    @ApiModelProperty(value="公众账号ID")
    private String appId;

    @ApiModelProperty(value="商户号")
    private String mchId;

    @ApiModelProperty(value="商户订单号")
    private String outTradeNo;

    @ApiModelProperty(value="第三方支付訂單號")
    private String transactionId;

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
