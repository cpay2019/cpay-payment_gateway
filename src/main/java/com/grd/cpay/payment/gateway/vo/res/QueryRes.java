package com.grd.cpay.payment.gateway.vo.res;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

public class QueryRes {

    @ApiModelProperty(value="公众账号ID")
    private String appId;

    @ApiModelProperty(value="商户号")
    private String mchId;

    @ApiModelProperty(value="设备号")
    private String deviceInfo;

    @ApiModelProperty(value="用户标识")
    private String openid;

    @ApiModelProperty(value="是否关注公众账号")
    private String isSubscribe;

    @ApiModelProperty(value="交易类型")
    private String tradeType;

    @ApiModelProperty(value="交易状态")
    private String tradeState;

    @ApiModelProperty(value="付款银行")
    private String bankType;

    @ApiModelProperty(value="标价金额")
    private BigDecimal totalFee;

    @ApiModelProperty(value="应结订单金额")
    private BigDecimal settlementTotalFee;

    @ApiModelProperty(value="清算編號")
    private String settlementId;

    @ApiModelProperty(value="标价币种")
    private String feeType;

    @ApiModelProperty(value="现金支付金额")
    private BigDecimal cashFee;

    @ApiModelProperty(value="现金支付币种")
    private String cashFeeType;

    @ApiModelProperty(value="代金券金额")
    private BigDecimal couponFee;

    @ApiModelProperty(value="代金券使用数量")
    private int couponCount;

    @ApiModelProperty(value="微信订单号")
    private String transactionId;

    @ApiModelProperty(value="商户订单号")
    private String outTradeNo;

    @ApiModelProperty(value="附加数据")
    private String attach;

    @ApiModelProperty(value="支付完成时间")
    private Date timeEnd;

    @ApiModelProperty(value="交易状态描述")
    private String tradeStateDesc;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getIsSubscribe() {
        return isSubscribe;
    }

    public void setIsSubscribe(String isSubscribe) {
        this.isSubscribe = isSubscribe;
    }

    public String getTradeState() {
        return tradeState;
    }

    public void setTradeState(String tradeState) {
        this.tradeState = tradeState;
    }

    public String getBankType() {
        return bankType;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getCashFeeType() {
        return cashFeeType;
    }

    public void setCashFeeType(String cashFeeType) {
        this.cashFeeType = cashFeeType;
    }

    public int getCouponCount() {
        return couponCount;
    }

    public void setCouponCount(int couponCount) {
        this.couponCount = couponCount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getTradeStateDesc() {
        return tradeStateDesc;
    }

    public void setTradeStateDesc(String tradeStateDesc) {
        this.tradeStateDesc = tradeStateDesc;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public BigDecimal getSettlementTotalFee() {
        return settlementTotalFee;
    }

    public void setSettlementTotalFee(BigDecimal settlementTotalFee) {
        this.settlementTotalFee = settlementTotalFee;
    }

    public BigDecimal getCouponFee() {
        return couponFee;
    }

    public void setCouponFee(BigDecimal couponFee) {
        this.couponFee = couponFee;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getSettlementId() {
        return settlementId;
    }

    public void setSettlementId(String settlementId) {
        this.settlementId = settlementId;
    }

    public BigDecimal getCashFee() {
        return cashFee;
    }

    public void setCashFee(BigDecimal cashFee) {
        this.cashFee = cashFee;
    }
}
