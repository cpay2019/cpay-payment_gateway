package com.grd.cpay.payment.gateway.vo.req;

import io.swagger.annotations.ApiModelProperty;
import com.grd.cpay.base.enums.ChannelType;

import java.math.BigDecimal;
import java.util.Date;

public class QueryReq {

    @ApiModelProperty(value="支付渠道")
    private ChannelType channelType;

    @ApiModelProperty(value="API Key")
    private String apiKey;

    @ApiModelProperty(value="公众账号ID")
    private String appId;

    @ApiModelProperty(value="商户号")
    private String mchId;

    @ApiModelProperty(value="设备号")
    private String deviceInfo;

    @ApiModelProperty(value="商品描述")
    private String body;

    @ApiModelProperty(value="商品详情")
    private String detail;

    @ApiModelProperty(value="附加数据")
    private String attach;


    /*
    二选一
    微信的订单号，建议优先使用
    * */
    @ApiModelProperty(value="微信订单号")
    private String transactionId;//查询订单orderquery -> 微信的订单号，建议优先使用

    @ApiModelProperty(value="商户订单号")
    private String outTradeNo;




    @ApiModelProperty(value="标价币种")
    private String feeType;

    @ApiModelProperty(value="标价金额")
    private BigDecimal totalFee;

    @ApiModelProperty(value="终端IP")
    private String spbillCreateIp;

    @ApiModelProperty(value="交易起始时间")
    private Date timeStart;

    @ApiModelProperty(value="交易结束时间")
    private Date timeExpire;

    @ApiModelProperty(value="通知地址")
    private String notifyUrl;

    @ApiModelProperty(value="商品ID")
    private String productId;

    @ApiModelProperty(value="场景信息")
    private String sceneInfo;

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getSpbillCreateIp() {
        return spbillCreateIp;
    }

    public void setSpbillCreateIp(String spbillCreateIp) {
        this.spbillCreateIp = spbillCreateIp;
    }

    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(Date timeExpire) {
        this.timeExpire = timeExpire;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSceneInfo() {
        return sceneInfo;
    }

    public void setSceneInfo(String sceneInfo) {
        this.sceneInfo = sceneInfo;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
