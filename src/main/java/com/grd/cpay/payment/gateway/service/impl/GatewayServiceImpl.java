package com.grd.cpay.payment.gateway.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.response.*;
import com.grd.cpay.base.vo.BaseResStatus;
import com.grd.cpay.base.vo.BaseResponse;
import com.grd.cpay.payment.alipay.service.AlipayService;
import com.grd.cpay.payment.alipay.vo.req.*;
import com.grd.cpay.payment.gateway.service.GatewayService;
import com.grd.cpay.payment.gateway.vo.req.*;
import com.grd.cpay.payment.gateway.vo.res.CancelRes;
import com.grd.cpay.payment.gateway.vo.res.CloseRes;
import com.grd.cpay.payment.gateway.vo.res.OrderRes;
import com.grd.cpay.payment.gateway.vo.res.QueryRes;
import com.grd.cpay.payment.union.service.UnionService;
import com.grd.cpay.payment.union.vo.req.UnionApplyQrCodeReq;
import com.grd.cpay.payment.union.vo.req.UnionOrderQueryReq;
import com.grd.cpay.payment.union.vo.res.UnionApplyQrCodeRes;
import com.grd.cpay.payment.union.vo.res.UnionOrderQueryRes;
import com.grd.cpay.payment.wechat.service.WechatService;
import com.grd.cpay.payment.wechat.utils.WechatUtils;
import com.grd.cpay.payment.wechat.vo.req.CloseOrderReq;
import com.grd.cpay.payment.wechat.vo.req.DownloadBillReq;
import com.grd.cpay.payment.wechat.vo.req.OrderQueryReq;
import com.grd.cpay.payment.wechat.vo.req.UnifiedOrderReq;
import com.grd.cpay.payment.wechat.vo.res.UnifiedOrderRes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Service
public class GatewayServiceImpl implements GatewayService {

    @Value("${alipay.notifyUrl}")
    private String NOTIFY_URL;

    @Autowired
    private WechatService wechatService;

    @Autowired
    private AlipayService alipayService;

    @Autowired
    private UnionService unionService;

    @Override
    public BaseResponse<OrderRes> order(OrderReq orderReq) throws AlipayApiException, IOException {

        BaseResponse<OrderRes> orderResBaseResponse = new BaseResponse<>(BaseResStatus.FAIL);
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        switch (orderReq.getChannelType()) {
            case WECHAT:
                UnifiedOrderReq unifiedOrderReq = new UnifiedOrderReq();
                unifiedOrderReq.setAppid(orderReq.getAppId());
                unifiedOrderReq.setMchId(orderReq.getMchId());
                unifiedOrderReq.setDeviceInfo(orderReq.getDeviceInfo());
                unifiedOrderReq.setBody(orderReq.getBody());
                unifiedOrderReq.setDetail(orderReq.getDetail());
                unifiedOrderReq.setAttach(orderReq.getAttach());
                unifiedOrderReq.setOutTradeNo(orderReq.getOutTradeNo());
                unifiedOrderReq.setFeeType(orderReq.getFeeType());
                unifiedOrderReq.setTotalFee(orderReq.getTotalFee());
                unifiedOrderReq.setSpbillCreateIp(orderReq.getSpbillCreateIp());
                unifiedOrderReq.setTimeStart(df.format(orderReq.getTimeStart()));
                unifiedOrderReq.setTimeExpire(df.format(orderReq.getTimeExpire()));
                unifiedOrderReq.setNotifyUrl(orderReq.getNotifyUrl());
                unifiedOrderReq.setTradeType("NATIVE");
                unifiedOrderReq.setProductId(orderReq.getProductId());
                unifiedOrderReq.setSceneInfo(orderReq.getSceneInfo());
                unifiedOrderReq.setNonceStr(WechatUtils.genNonceStr());
                UnifiedOrderRes unifiedOrderRes = wechatService.unifiedOrder(unifiedOrderReq, orderReq.getApiKey());
                if(unifiedOrderReq.getReturnCode().equals("FAIL")) {
                    System.out.println("OK!");
                }
                break;
            case ALI:
                PrecreateReq precreateReq = new PrecreateReq();
                precreateReq.setOut_trade_no(orderReq.getOutTradeNo());
                precreateReq.setTotal_amount(orderReq.getTotalFee());
                precreateReq.setSubject(orderReq.getProductId());
                AlipayTradePrecreateResponse response = alipayService.precreate(precreateReq, orderReq.getAppId(), orderReq.getApiKey(), NOTIFY_URL);
                if(response!=null && response.isSuccess()) {
                    OrderRes orderRes = new OrderRes();
                    orderRes.setOutTradeNo(response.getOutTradeNo());
                    orderRes.setCodeUrl(response.getQrCode());
                    orderResBaseResponse = new BaseResponse<>(orderRes);
                } else {
                    orderResBaseResponse.setMessage("支付寶調用交易失敗");
                }
                break;
            case UNION:
                UnionApplyQrCodeReq unionApplyQrCodeReq = new UnionApplyQrCodeReq();
                unionApplyQrCodeReq.setMerId(orderReq.getMchId());
                unionApplyQrCodeReq.setOrderId(orderReq.getOutTradeNo());
                unionApplyQrCodeReq.setTxnAmt(orderReq.getTotalFee());
                unionApplyQrCodeReq.setTxnTime(df.format(orderReq.getTimeStart()));
                UnionApplyQrCodeRes unionApplyQrCodeRes = unionService.applyQrCode(unionApplyQrCodeReq);
                if(unionApplyQrCodeRes!=null && unionApplyQrCodeRes.getRespCode().equals("00")) {
                    OrderRes orderRes = new OrderRes();
                    orderRes.setAppId(orderReq.getAppId());
                    orderRes.setMchId(orderReq.getMchId());
                    orderRes.setOutTradeNo(unionApplyQrCodeRes.getOutTradeNo());
                    orderRes.setCodeUrl(unionApplyQrCodeRes.getCodeUrl());
                    orderResBaseResponse = new BaseResponse<>(orderRes);
                } else {
                    orderResBaseResponse.setMessage("UNION PAY 調用交易失敗");
                }
            default:
        }

        return orderResBaseResponse;
    }

    @Override
    public BaseResponse<QueryRes> orderquery(QueryReq req) throws AlipayApiException, IOException {

        BaseResponse<QueryRes> orderResBaseResponse = new BaseResponse<>(BaseResStatus.FAIL);
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        switch (req.getChannelType()) {
            case WECHAT:
                OrderQueryReq orderQueryReq = new OrderQueryReq();
                orderQueryReq.setAppid(req.getAppId());
                orderQueryReq.setMchId(req.getMchId());
                orderQueryReq.setNonceStr(WechatUtils.genNonceStr());
                orderQueryReq.setOutTradeNo(req.getOutTradeNo());
                orderQueryReq.setTransactionId(req.getTransactionId());//微信的订单号，建议优先使用
                wechatService.orderquery(orderQueryReq, req.getApiKey());
                break;
            case ALI:
                AliQueryReq aliQueryReq = new AliQueryReq();
                aliQueryReq.setOut_trade_no(req.getOutTradeNo());
                aliQueryReq.setTrade_no(req.getTransactionId());
                AlipayTradeQueryResponse response = alipayService.query(aliQueryReq, req.getAppId(), req.getApiKey());
                if(response!=null && response.isSuccess()) {
                    QueryRes queryRes = new QueryRes();
                    queryRes.setAppId(req.getAppId());
                    queryRes.setTotalFee(response.getTotalAmount()!=null?new BigDecimal(response.getTotalAmount()):null);
                    queryRes.setCouponFee(response.getPointAmount()!=null?new BigDecimal(response.getPointAmount()):null);
                    queryRes.setTransactionId(response.getTradeNo());
                    queryRes.setOutTradeNo(response.getOutTradeNo());
                    queryRes.setTradeState(response.getTradeStatus());
                    queryRes.setTimeEnd(response.getSendPayDate());
                    queryRes.setSettlementTotalFee(response.getSettleAmount()!=null?new BigDecimal(response.getSettleAmount()):null);
                    queryRes.setSettlementId(response.getSettlementId());
                    queryRes.setCashFee(response.getBuyerPayAmount()!=null? new BigDecimal(response.getBuyerPayAmount()):null);
                    orderResBaseResponse = new BaseResponse<>(queryRes);
                } else {
                    orderResBaseResponse.setMessage("支付寶調用交易失敗");
                }
                break;
            case UNION:
                UnionOrderQueryReq unionOrderQueryReq = new UnionOrderQueryReq();
                unionOrderQueryReq.setMerId(req.getMchId());
                unionOrderQueryReq.setOrderId(req.getOutTradeNo());
                unionOrderQueryReq.setTxnTime(df.format(req.getTimeStart()));
                UnionOrderQueryRes unionOrderQueryRes = unionService.orderQuery(unionOrderQueryReq);
                if(unionOrderQueryRes!=null && unionOrderQueryRes.getRespCode().equals("00")) {
                    QueryRes queryRes = new QueryRes();
                    queryRes.setMchId(unionOrderQueryRes.getMerId());
                    queryRes.setOutTradeNo(unionOrderQueryRes.getOrderId());
                    queryRes.setTradeState(unionOrderQueryRes.getOrigRespCode().equals("00")?"TRADE_SUCCESS":"WAITING");
                    orderResBaseResponse = new BaseResponse<>(queryRes);
                } else {
                    orderResBaseResponse.setMessage("Union Pay 調用交易失敗");
                }

                break;
            default:
        }

        return orderResBaseResponse;
    }

    @Override
    public BaseResponse<CloseRes> closeorder(CloseReq req) throws AlipayApiException {

        BaseResponse<CloseRes> orderResBaseResponse = new BaseResponse<>(BaseResStatus.FAIL);

        switch (req.getChannelType()) {
            case WECHAT:
                CloseOrderReq closeOrderReq = new CloseOrderReq();
                closeOrderReq.setAppid(req.getAppId());
                closeOrderReq.setMchId(req.getMchId());
                closeOrderReq.setNonceStr(WechatUtils.genNonceStr());
                closeOrderReq.setOutTradeNo(req.getOutTradeNo());
                wechatService.closeorder(closeOrderReq, req.getApiKey());
                break;
            case ALI:
                AliCloseReq aliCloseReq = new AliCloseReq();
                aliCloseReq.setOut_trade_no(req.getOutTradeNo());
                aliCloseReq.setTrade_no(req.getTransactionId());
                AlipayTradeCloseResponse response = alipayService.close(aliCloseReq, req.getAppId(), req.getApiKey());
                if(response!=null && response.isSuccess()) {
                    CloseRes closeRes = new CloseRes();
                    closeRes.setAppId(req.getAppId());
                    orderResBaseResponse = new BaseResponse<>(closeRes);
                } else {
                    orderResBaseResponse.setMessage("支付寶調用交易失敗");
                }
                break;
            default:
        }

        return orderResBaseResponse;
    }

    @Override
    public BaseResponse<CancelRes> cancelder(CancelReq req) throws AlipayApiException {

        BaseResponse<CancelRes> orderResBaseResponse = new BaseResponse<>(BaseResStatus.FAIL);

        switch (req.getChannelType()) {
            case ALI:
                AliCancelReq aliCancelReq = new AliCancelReq();
                aliCancelReq.setOut_trade_no(req.getOutTradeNo());
                aliCancelReq.setTrade_no(req.getTransactionId());
                AlipayTradeCancelResponse response = alipayService.cancel(aliCancelReq, req.getAppId(), req.getApiKey());
                if(response!=null && response.isSuccess()) {
                    CancelRes cancelRes = new CancelRes();
                    cancelRes.setAppId(req.getAppId());
                    orderResBaseResponse = new BaseResponse<>(cancelRes);
                } else {
                    orderResBaseResponse.setMessage("支付寶調用交易失敗");
                }
                break;
            default:
        }
        return orderResBaseResponse;
    }

    @Override
    public BaseResponse<OrderRes> downloadbill(BillReq req) throws AlipayApiException {

        BaseResponse<OrderRes> orderResBaseResponse = new BaseResponse<>();

        switch (req.getChannelType()) {
            case WECHAT:
                DownloadBillReq downloadBillReq = new DownloadBillReq();
                downloadBillReq.setAppid(req.getAppId());
                downloadBillReq.setMchId(req.getMchId());
                downloadBillReq.setNonceStr(WechatUtils.genNonceStr());
                downloadBillReq.setBillDate(req.getBillDate());
                downloadBillReq.setBillType(req.getBillType());
                downloadBillReq.setTarType(req.getTarType());
//                closeOrderReq.setSignType("MD5");//非必填 - 签名类型，目前支持HMAC-SHA256和MD5，默认为MD5
                wechatService.downloadbill(downloadBillReq, req.getApiKey());
                break;
            case ALI:
                AliDownloadBillReq aliDownloadBillReq = new AliDownloadBillReq();
                aliDownloadBillReq.setBill_type(req.getBillType());
                String billDateStr = "";
                if(req.getBillDate().length()==6) {
                    billDateStr = req.getBillDate().substring(0,4) + "-" + req.getBillDate().substring(4);
                } else if(req.getBillDate().length()==8) {
                    billDateStr = req.getBillDate().substring(0,4) + "-" + req.getBillDate().substring(4,6) + "-" + req.getBillDate().substring(6);
                }
                aliDownloadBillReq.setBill_date(billDateStr);
                AlipayDataDataserviceBillDownloadurlQueryResponse response = alipayService.downloadbill(aliDownloadBillReq, req.getAppId(), req.getApiKey());
                if(response!=null && response.isSuccess()) {
                    System.out.println(response.getBillDownloadUrl());
                } else {
                    orderResBaseResponse.setMessage("支付寶調用交易失敗");
                }
            default:
        }

        return orderResBaseResponse;
    }

}
