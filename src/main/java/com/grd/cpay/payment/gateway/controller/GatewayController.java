package com.grd.cpay.payment.gateway.controller;

import com.alipay.api.AlipayApiException;
import com.grd.cpay.base.controller.BaseController;
import com.grd.cpay.base.vo.BaseResponse;
import com.grd.cpay.payment.gateway.service.GatewayService;
import com.grd.cpay.payment.gateway.vo.req.*;
import com.grd.cpay.payment.gateway.vo.res.CancelRes;
import com.grd.cpay.payment.gateway.vo.res.CloseRes;
import com.grd.cpay.payment.gateway.vo.res.OrderRes;
import com.grd.cpay.payment.gateway.vo.res.QueryRes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;

@RestController
@RequestMapping("/gateway")
@Api(tags = "Gateway Apis")
public class GatewayController extends BaseController {

    @Autowired
    private GatewayService gatewayService;

    @RequestMapping(value = "/order", method = {RequestMethod.POST})
    @ApiOperation("統一訂單")
    public @ResponseBody BaseResponse<OrderRes> order(@RequestBody OrderReq orderReq) throws AlipayApiException, IOException {
        return gatewayService.order(orderReq);
    }

    @RequestMapping(value = "/orderquery", method = {RequestMethod.POST})
    @ApiOperation("查詢訂單")
    public @ResponseBody
    BaseResponse<QueryRes> orderquery(@RequestBody QueryReq orderReq) throws AlipayApiException, IOException {
        return gatewayService.orderquery(orderReq);
    }

    @RequestMapping(value = "/closeorder", method = {RequestMethod.POST})
    @ApiOperation("關閉訂單")
    public @ResponseBody
    BaseResponse<CloseRes> closeorder(@RequestBody CloseReq closeReq) throws AlipayApiException {
        return gatewayService.closeorder(closeReq);
    }

    @RequestMapping(value = "/cancelorder", method = {RequestMethod.POST})
    @ApiOperation("取消訂單")
    public @ResponseBody
    BaseResponse<CancelRes> cancelorder(@RequestBody CancelReq cancelReq) throws AlipayApiException {
        return gatewayService.cancelder(cancelReq);
    }

    @RequestMapping(value = "/downloadbill", method = {RequestMethod.POST})
    @ApiOperation("下載對帳單")
    public @ResponseBody BaseResponse<OrderRes> downloadbill(@RequestBody BillReq downloadBillReq) throws AlipayApiException {
        return gatewayService.downloadbill(downloadBillReq);
    }
}
