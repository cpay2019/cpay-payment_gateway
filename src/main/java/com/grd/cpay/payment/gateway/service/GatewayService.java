package com.grd.cpay.payment.gateway.service;

import com.alipay.api.AlipayApiException;
import com.grd.cpay.base.vo.BaseResponse;
import com.grd.cpay.payment.gateway.vo.req.*;
import com.grd.cpay.payment.gateway.vo.res.CancelRes;
import com.grd.cpay.payment.gateway.vo.res.CloseRes;
import com.grd.cpay.payment.gateway.vo.res.OrderRes;
import com.grd.cpay.payment.gateway.vo.res.QueryRes;

import java.io.IOException;

public interface GatewayService {

    /**
     * 統一訂單
     *
     * @param orderReq
     * @return
     */
    BaseResponse<OrderRes> order(OrderReq orderReq) throws AlipayApiException, IOException;

    /**
     * 查询订单
     *
     * @param orderReq
     * @return
     */
    BaseResponse<QueryRes> orderquery(QueryReq orderReq) throws AlipayApiException, IOException;

    /**
     * 关闭订单
     *
     * @param closeReq
     * @return
     */
    BaseResponse<CloseRes> closeorder(CloseReq closeReq) throws AlipayApiException;

    /**
     * 取消訂單
     *
     * @param cancelReq
     * @return
     * @throws AlipayApiException
     */
    BaseResponse<CancelRes> cancelder(CancelReq cancelReq) throws AlipayApiException;

    /**
     * 下载对账单
     *
     * @param downloadBillReq
     * @return
     */
    BaseResponse<OrderRes> downloadbill(BillReq downloadBillReq) throws AlipayApiException;

}
