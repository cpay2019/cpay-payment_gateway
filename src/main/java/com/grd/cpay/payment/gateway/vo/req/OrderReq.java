package com.grd.cpay.payment.gateway.vo.req;

import io.swagger.annotations.ApiModelProperty;
import com.grd.cpay.base.enums.ChannelType;

import java.math.BigDecimal;
import java.util.Date;

public class OrderReq {

    @ApiModelProperty(value="支付渠道", example = "UNION")
    private ChannelType channelType;

    @ApiModelProperty(value="API Key(private key)", hidden = true)
    private String apiKey;

    @ApiModelProperty(value="公众账号ID", example = "700000000000001")
    private String appId;

    @ApiModelProperty(value="商户号", example = "700000000000001")
    private String mchId;

    @ApiModelProperty(value="设备号", hidden = true)
    private String deviceInfo;

    @ApiModelProperty(value="商品描述", hidden = true)
    private String body;

    @ApiModelProperty(value="商品详情", hidden = true)
    private String detail;

    @ApiModelProperty(value="附加数据", hidden = true)
    private String attach;

    @ApiModelProperty(value="商户订单号")
    private String outTradeNo;

    @ApiModelProperty(value="标价币种", hidden = true)
    private String feeType;

    @ApiModelProperty(value="标价金额", example = "100")
    private BigDecimal totalFee;

    @ApiModelProperty(value="终端IP", hidden = true)
    private String spbillCreateIp;

    @ApiModelProperty(value="交易起始时间")
    private Date timeStart;

    @ApiModelProperty(value="交易结束时间", hidden = true)
    private Date timeExpire;

    @ApiModelProperty(value="通知地址", hidden = true)
    private String notifyUrl;

    @ApiModelProperty(value="商品ID", example = "productID")
    private String productId;

    @ApiModelProperty(value="场景信息", hidden = true)
    private String sceneInfo;

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getSpbillCreateIp() {
        return spbillCreateIp;
    }

    public void setSpbillCreateIp(String spbillCreateIp) {
        this.spbillCreateIp = spbillCreateIp;
    }

    public Date getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Date timeStart) {
        this.timeStart = timeStart;
    }

    public Date getTimeExpire() {
        return timeExpire;
    }

    public void setTimeExpire(Date timeExpire) {
        this.timeExpire = timeExpire;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSceneInfo() {
        return sceneInfo;
    }

    public void setSceneInfo(String sceneInfo) {
        this.sceneInfo = sceneInfo;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
