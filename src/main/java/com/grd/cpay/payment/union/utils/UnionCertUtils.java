package com.grd.cpay.payment.union.utils;

import com.grd.cpay.base.utils.LogUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

public class UnionCertUtils {

    private static final String TAG = "UnionCertUtils";

    public static String getCertIdByKeyStoreMap(String certPath, String certPwd) throws IOException {
        init();
        KeyStore keyStore = getKeyInfo(certPath, certPwd, "PKCS12");
        return getCertIdIdByStore(keyStore);
    }

    public static PrivateKey getSignCertPrivateKeyByStoreMap(String certPath, String certPwd) {

        try {
            KeyStore keyStore = getKeyInfo(certPath, certPwd, "PKCS12");
            Enumeration<String> aliasenum = keyStore.aliases();
            String keyAlias = null;
            if (aliasenum.hasMoreElements()) {
                keyAlias = aliasenum.nextElement();
            }
            PrivateKey privateKey = (PrivateKey) keyStore.getKey(keyAlias, certPwd.toCharArray());
            return privateKey;
        } catch (KeyStoreException e) {
            LogUtils.error(TAG, "getSignCertPrivateKeyByStoreMap Error" + e);
            return null;
        } catch (UnrecoverableKeyException e) {
            LogUtils.error(TAG, "getSignCertPrivateKeyByStoreMap Error" + e);
            return null;
        } catch (NoSuchAlgorithmException e) {
            LogUtils.error(TAG, "getSignCertPrivateKeyByStoreMap Error" + e);
            return null;
        } catch (IOException e) {
            LogUtils.error(TAG, "getSignCertPrivateKeyByStoreMap Error" + e);
            return null;
        }
    }

    /**
     * 将字符串转换为X509Certificate对象.
     *
     * @param x509CertString
     * @return
     */
    public static X509Certificate genCertificateByStr(String x509CertString) {
        X509Certificate x509Cert = null;
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
            InputStream tIn = new ByteArrayInputStream(
                    x509CertString.getBytes("ISO-8859-1"));
            x509Cert = (X509Certificate) cf.generateCertificate(tIn);
        } catch (Exception e) {
            LogUtils.error(TAG, "gen certificate error : " + e);
        }
        return x509Cert;
    }

    /**
     * 检查证书链
     *
     * @param cert
     *            待验证的证书
     * @return
     */
    public static boolean verifyCertificate(String middleCertPath, String rootCertPath, X509Certificate cert) {

        if ( null == cert) {
            LogUtils.error(TAG, "cert must Not null");
            return false;
        }
        try {
            cert.checkValidity();//验证有效期
//			cert.verify(middleCert.getPublicKey());
            if(!verifyCertificateChain(middleCertPath, rootCertPath, cert)){
                return false;
            }
        } catch (Exception e) {
            LogUtils.error(TAG, "verifyCertificate fail : " + e);
            return false;
        }

//        // 验证公钥是否属于银联
//        if(!"中国银联股份有限公司".equals(getIdentitiesFromCertficate(cert))) {
//            LogUtils.error(TAG, "cer owner is not CUP:" + getIdentitiesFromCertficate(cert));
//            return false;
//        }
        return true;
    }

    /**
     * 获取证书的CN
     * @param aCert
     * @return
     */
    private static String getIdentitiesFromCertficate(X509Certificate aCert) {
        String tDN = aCert.getSubjectDN().toString();
        String tPart = "";
        if ((tDN != null)) {
            String tSplitStr[] = tDN.substring(tDN.indexOf("CN=")).split("@");
            if (tSplitStr != null && tSplitStr.length > 2
                    && tSplitStr[2] != null)
                tPart = tSplitStr[2];
        }
        return tPart;
    }

    /**
     * 验证书链。
     * @param cert
     * @return
     */
    private static boolean verifyCertificateChain(String middleCertPath, String rootCertPath, X509Certificate cert){

        if ( null == cert) {
            LogUtils.error(TAG, "cert must Not null");
            return false;
        }

        X509Certificate middleCert = getMiddleCert(middleCertPath);
        if (null == middleCert) {
            LogUtils.error(TAG, "middleCert must Not null");
            return false;
        }

        X509Certificate rootCert = getRootCert(rootCertPath);
        if (null == rootCert) {
            LogUtils.error(TAG, "rootCert or cert must Not null");
            return false;
        }

        try {

            X509CertSelector selector = new X509CertSelector();
            selector.setCertificate(cert);

            Set<TrustAnchor> trustAnchors = new HashSet<TrustAnchor>();
            trustAnchors.add(new TrustAnchor(rootCert, null));
            PKIXBuilderParameters pkixParams = new PKIXBuilderParameters(
                    trustAnchors, selector);

            Set<X509Certificate> intermediateCerts = new HashSet<X509Certificate>();
            intermediateCerts.add(rootCert);
            intermediateCerts.add(middleCert);
            intermediateCerts.add(cert);

            pkixParams.setRevocationEnabled(false);

            CertStore intermediateCertStore = CertStore.getInstance("Collection",
                    new CollectionCertStoreParameters(intermediateCerts), "BC");
            pkixParams.addCertStore(intermediateCertStore);

            CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "BC");

            @SuppressWarnings("unused")
            PKIXCertPathBuilderResult result = (PKIXCertPathBuilderResult) builder
                    .build(pkixParams);
            LogUtils.info(TAG, "verify certificate chain succeed.");
            return true;
        } catch (java.security.cert.CertPathBuilderException e){
            LogUtils.error(TAG, "verify certificate chain fail. : " + e);
        } catch (Exception e) {
            LogUtils.error(TAG, "verify certificate chain exception : " + e);
        }
        return false;
    }

    /**
     * 从配置文件acp_sdk.properties中获取验签公钥使用的中级证书
     * @return
     */
    public static X509Certificate getMiddleCert(String path) {
        return initCert(path);
    }

    private static void init() {
        if (Security.getProvider("BC") == null) {
            LogUtils.info(TAG, "add BC provider");
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        } else {
            Security.removeProvider("BC"); //解决eclipse调试时tomcat自动重新加载时，BC存在不明原因异常的问题。
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            LogUtils.info(TAG, "re-add BC provider");
        }
    }

    /**
     * 从配置文件acp_sdk.properties中获取验签公钥使用的根证书
     * @return
     */
    public static X509Certificate getRootCert(String path) {
        return initCert(path);
    }

    /**
     * 通过证书路径初始化为公钥证书
     * @param path
     * @return
     */
    private static X509Certificate initCert(String path) {
        X509Certificate encryptCertTemp = null;
        CertificateFactory cf = null;
        InputStream in = null;
        try {
            cf = CertificateFactory.getInstance("X.509", "BC");

            Resource resource = new ClassPathResource(path);
            in = resource.getInputStream();

            encryptCertTemp = (X509Certificate) cf.generateCertificate(in);
            // 打印证书加载信息,供测试阶段调试
            LogUtils.info(TAG, "[" + path + "][CertId="
                    + encryptCertTemp.getSerialNumber().toString() + "]");
        } catch (CertificateException e) {
            LogUtils.error(TAG, "InitCert Error : " + e);
        } catch (FileNotFoundException e) {
            LogUtils.error(TAG, "InitCert Error File Not Found : " + e);
        } catch (NoSuchProviderException e) {
            LogUtils.error(TAG, "LoadVerifyCert Error No BC Provider : " + e);
        } catch (IOException e) {
            LogUtils.error(TAG, "IOException : " + e);
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    LogUtils.error(TAG, e.toString());
                }
            }
        }
        return encryptCertTemp;
    }

    private static KeyStore getKeyInfo(String pfxkeyfile, String keypwd, String type) throws IOException {

        LogUtils.info(TAG, "加载签名证书==>" + pfxkeyfile);
        InputStream fis = null;
        try {
            Resource resource = new ClassPathResource(pfxkeyfile);
            fis = resource.getInputStream();
            KeyStore ks = KeyStore.getInstance(type, "BC");
            LogUtils.info(TAG, "Load RSA CertPath=[" + pfxkeyfile + "],Pwd=["+ keypwd + "],type=["+type+"]");
            char[] nPassword = null;
            nPassword = null == keypwd || "".equals(keypwd.trim()) ? null: keypwd.toCharArray();
            if (null != ks) {
                ks.load(fis, nPassword);
            }
            return ks;
        } catch (Exception e) {
            LogUtils.error(TAG, "getKeyInfo Error" + e);
            return null;
        } finally {
            if(null!=fis)
                fis.close();
        }
    }

    private static String getCertIdIdByStore(KeyStore keyStore) {
        Enumeration<String> aliasenum = null;
        try {
            aliasenum = keyStore.aliases();
            String keyAlias = null;
            if (aliasenum.hasMoreElements()) {
                keyAlias = aliasenum.nextElement();
            }
            X509Certificate cert = (X509Certificate) keyStore
                    .getCertificate(keyAlias);
            return cert.getSerialNumber().toString();
        } catch (KeyStoreException e) {
            LogUtils.error(TAG, "getCertIdIdByStore Error" + e);
            return null;
        }
    }
}
