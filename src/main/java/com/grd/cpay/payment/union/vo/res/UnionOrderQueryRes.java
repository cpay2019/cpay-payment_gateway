package com.grd.cpay.payment.union.vo.res;

import com.grd.cpay.payment.union.vo.UnionBaseRes;

public class UnionOrderQueryRes extends UnionBaseRes {

    private String merId;
    private String orderId;
    private String origRespCode;
    private String origRespMsg;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrigRespCode() {
        return origRespCode;
    }

    public void setOrigRespCode(String origRespCode) {
        this.origRespCode = origRespCode;
    }

    public String getOrigRespMsg() {
        return origRespMsg;
    }

    public void setOrigRespMsg(String origRespMsg) {
        this.origRespMsg = origRespMsg;
    }

    public String getMerId() {
        return merId;
    }

    public void setMerId(String merId) {
        this.merId = merId;
    }
}
