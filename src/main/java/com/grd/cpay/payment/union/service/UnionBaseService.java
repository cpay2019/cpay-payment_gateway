package com.grd.cpay.payment.union.service;


import com.grd.cpay.base.utils.HttpClient;
import com.grd.cpay.base.utils.LogUtils;
import com.grd.cpay.payment.union.utils.SDKUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.grd.cpay.payment.union.utils.SDKUtil.*;

public class UnionBaseService {

    private static final String TAG = "UnionBaseService";

    public Map<String, String> sign(Map<String, String> reqData, String certPath,
                                           String certPwd, String encoding) throws IOException {
        reqData = filterBlank(reqData);
        signByCertInfo(reqData,certPath,certPwd,encoding);
        return reqData;
    }

    /**
     * 功能：后台交易提交请求报文并接收同步应答报文<br>
     * @param reqData 请求报文<br>
     * @param rspData 应答报文<br>
     * @param reqUrl  请求地址<br>
     * @param encoding<br>
     * @return 应答http 200返回true ,其他false<br>
     */
    public Map<String,String> post(Map<String, String> reqData,String reqUrl,String encoding) {
        Map<String, String> rspData = new HashMap<String,String>();

        LogUtils.info(TAG, "请求银联地址:" + reqUrl);
        //发送后台请求数据
        HttpClient hc = new HttpClient(reqUrl, 30000, 30000);
        try {
            int status = hc.send(reqData, encoding);
            if (200 == status) {
                String resultString = hc.getResult();
                if (null != resultString && !"".equals(resultString)) {
                    // 将返回结果转换为map
                    Map<String,String> tmpRspData  = convertResultStringToMap(resultString);
                    rspData.putAll(tmpRspData);
                }
            }else{
                LogUtils.info(TAG, "返回http状态码["+status+"]，请检查请求报文或者请求地址是否正确");
            }
        } catch (Exception e) {
            LogUtils.error(TAG, e.getMessage()+":"+e);
        }
        return rspData;
    }

    /**
     * 验证签名(SHA-1摘要算法)<br>
     * @param rspData 返回报文数据<br>
     * @param encoding 上送请求报文域encoding字段的值<br>
     * @return true 通过 false 未通过<br>
     */
    public boolean validate(Map<String, String> rspData, String middleCertPath, String rootCertPath, String encoding) {
        return SDKUtil.validate(rspData, middleCertPath, rootCertPath, encoding);
    }
}
