package com.grd.cpay.payment.union.vo.res;

import com.grd.cpay.payment.union.vo.UnionBaseRes;

public class UnionApplyQrCodeRes extends UnionBaseRes {

    private String outTradeNo;
    private String codeUrl;

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getCodeUrl() {
        return codeUrl;
    }

    public void setCodeUrl(String codeUrl) {
        this.codeUrl = codeUrl;
    }
}
