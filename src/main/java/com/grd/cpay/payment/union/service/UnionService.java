package com.grd.cpay.payment.union.service;

import com.grd.cpay.payment.union.vo.req.UnionApplyQrCodeReq;
import com.grd.cpay.payment.union.vo.req.UnionOrderQueryReq;
import com.grd.cpay.payment.union.vo.res.UnionApplyQrCodeRes;
import com.grd.cpay.payment.union.vo.res.UnionOrderQueryRes;

import java.io.IOException;

public interface UnionService {

    UnionApplyQrCodeRes applyQrCode(UnionApplyQrCodeReq unionApplyQrCodeReq) throws IOException;

    UnionOrderQueryRes orderQuery(UnionOrderQueryReq unionOrderQueryReq) throws IOException;
}
