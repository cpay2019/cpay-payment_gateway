package com.grd.cpay.payment.union.service.impl;

import com.grd.cpay.base.utils.LogUtils;
import com.grd.cpay.payment.union.service.UnionBaseService;
import com.grd.cpay.payment.union.service.UnionService;
import com.grd.cpay.payment.union.vo.req.UnionApplyQrCodeReq;
import com.grd.cpay.payment.union.vo.req.UnionOrderQueryReq;
import com.grd.cpay.payment.union.vo.res.UnionApplyQrCodeRes;
import com.grd.cpay.payment.union.vo.res.UnionOrderQueryRes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class UnionServiceImpl extends UnionBaseService implements UnionService {

    private static final String TAG = "UnionServiceImpl";

    private static final String VERSION = "5.1.0";
    private static final String ENCODING = "UTF-8";
    private static final String SIGN_METHOD = "01";

    @Value("${union.sign-cert}")
    private String SIGN_CERT_PATH;

    @Value("${union.middle-cert}")
    private String MIDDLE_CERT_PATH;

    @Value("${union.root-cert}")
    private String ROOT_CERT_PATH;

    @Value("${union.cert-pwd}")
    private String CERT_PWD;

    @Value("${union.notifyUrl}")
    private String NOTIFY_URL;

    @Value("${union.url}")
    private String URL;

    private static final String APPLY_QRCODE_URL = "/gateway/api/backTransReq.do";
    private static final String ORDER_QUERY_URL = "/gateway/api/queryTrans.do";

    @Override
    public UnionApplyQrCodeRes applyQrCode(UnionApplyQrCodeReq unionApplyQrCodeReq) throws IOException {
        Map<String, String> contentData = new HashMap<String, String>();

        contentData.put("version", VERSION);
        contentData.put("encoding", ENCODING);
        contentData.put("signMethod", SIGN_METHOD);
        contentData.put("txnType", "01");
        contentData.put("txnSubType", "07");
        contentData.put("bizType", "000000");
        contentData.put("channelType", "08");

        contentData.put("merId", unionApplyQrCodeReq.getMerId());
        contentData.put("accessType", "0");
        contentData.put("orderId", unionApplyQrCodeReq.getOrderId());
        contentData.put("txnTime", unionApplyQrCodeReq.getTxnTime());
        contentData.put("txnAmt", unionApplyQrCodeReq.getTxnAmt().multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_DOWN).toString());
        contentData.put("currencyCode", "156");
        contentData.put("backUrl", NOTIFY_URL);

        Map<String, String> reqData = sign(contentData, SIGN_CERT_PATH, CERT_PWD, ENCODING);
        Map<String, String> rspData = post(reqData,URL+APPLY_QRCODE_URL, ENCODING);
        if(!rspData.isEmpty()){
            if(validate(rspData, MIDDLE_CERT_PATH, ROOT_CERT_PATH, ENCODING)){
                LogUtils.info(TAG, "验证签名成功");
                String respCode = rspData.get("respCode");
                UnionApplyQrCodeRes unionApplyQrCodeRes = new UnionApplyQrCodeRes();
                unionApplyQrCodeRes.setRespCode(respCode);
                unionApplyQrCodeRes.setRespMsg(rspData.get("respMsg"));
                if(("00").equals(respCode)){
                    unionApplyQrCodeRes.setOutTradeNo(rspData.get("orderId"));
                    unionApplyQrCodeRes.setCodeUrl(rspData.get("qrCode"));

                }
                return unionApplyQrCodeRes;
            }else{
                LogUtils.error(TAG, "验证签名失败");
                UnionApplyQrCodeRes unionApplyQrCodeRes = new UnionApplyQrCodeRes();
                unionApplyQrCodeRes.setRespCode("99");
                unionApplyQrCodeRes.setRespMsg("验证签名失败");
                return unionApplyQrCodeRes;
            }
        }else{
            //未返回正确的http状态
            LogUtils.error(TAG, "未获取到返回报文或返回http状态码非200");
        }
        return null;
    }

    @Override
    public UnionOrderQueryRes orderQuery(UnionOrderQueryReq unionOrderQueryReq) throws IOException {
        Map<String, String> data = new HashMap<String, String>();

        data.put("version", VERSION);
        data.put("encoding", ENCODING);
        data.put("signMethod", SIGN_METHOD);
        data.put("txnType", "00");
        data.put("txnSubType", "00");
        data.put("bizType", "000201");

        data.put("merId", unionOrderQueryReq.getMerId());
        data.put("accessType", "0");
        data.put("orderId", unionOrderQueryReq.getOrderId());
        data.put("txnTime", unionOrderQueryReq.getTxnTime());


        Map<String, String> reqData = sign(data, SIGN_CERT_PATH, CERT_PWD, ENCODING);
        Map<String, String> rspData = post(reqData,URL+APPLY_QRCODE_URL, ENCODING);

        if(!rspData.isEmpty()){
            if(validate(rspData, MIDDLE_CERT_PATH, ROOT_CERT_PATH, ENCODING)){
                LogUtils.info(TAG, "验证签名成功");
                UnionOrderQueryRes unionOrderQueryRes = new UnionOrderQueryRes();
                unionOrderQueryRes.setRespCode(rspData.get("respCode"));
                unionOrderQueryRes.setRespMsg(rspData.get("respMsg"));
                if(("00").equals(rspData.get("respCode"))){//如果查询交易成功
                    String origRespCode = rspData.get("origRespCode");
                    unionOrderQueryRes.setOrigRespCode(origRespCode);
                    unionOrderQueryRes.setOrigRespMsg(rspData.get("origRespMsg"));
                    unionOrderQueryRes.setMerId(rspData.get("merId"));
                    if(("00").equals(origRespCode)){
                        //交易成功，更新商户订单状态
                        //TODO
                        return unionOrderQueryRes;
                    }else if(("03").equals(origRespCode)||
                            ("04").equals(origRespCode)||
                            ("05").equals(origRespCode)){
                        //订单处理中或交易状态未明，需稍后发起交易状态查询交易 【如果最终尚未确定交易是否成功请以对账文件为准】
                        return unionOrderQueryRes;
                    }else{
                        //其他应答码为交易失败
                        return unionOrderQueryRes;
                    }
                }else if(("34").equals(rspData.get("respCode"))){
                    //订单不存在，可认为交易状态未明，需要稍后发起交易状态查询，或依据对账结果为准
                    return unionOrderQueryRes;
                }else{//查询交易本身失败，如应答码10/11检查查询报文是否正确
                    return unionOrderQueryRes;
                }
            }else{
                LogUtils.error(TAG, "验证签名失败");
                UnionOrderQueryRes unionOrderQueryRes = new UnionOrderQueryRes();
                unionOrderQueryRes.setRespCode("99");
                unionOrderQueryRes.setRespMsg("验证签名失败");
                return unionOrderQueryRes;
            }
        }else{
            //未返回正确的http状态
            LogUtils.error(TAG, "未获取到返回报文或返回http状态码非200");
        }
        return null;
    }
}
