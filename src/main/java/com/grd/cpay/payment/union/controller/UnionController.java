package com.grd.cpay.payment.union.controller;

import com.grd.cpay.base.controller.BaseController;
import com.grd.cpay.base.utils.JsonUtils;
import com.grd.cpay.base.utils.LogUtils;
import com.grd.cpay.base.vo.BaseResponse;
import com.grd.cpay.payment.union.vo.req.UnionOrderinfoReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/union/callback")
@Api(tags = "Union Apis")
@ApiIgnore
public class UnionController extends BaseController {

    @RequestMapping(value = "/orderinfo", method = {RequestMethod.POST})
    @ApiOperation("訂單訊息更新")
    public @ResponseBody
    BaseResponse orderinfo(UnionOrderinfoReq unionOrderinfoReq) {
        LogUtils.info(getTag(), JsonUtils.fromObject(unionOrderinfoReq));
        return new BaseResponse();
    }
}
