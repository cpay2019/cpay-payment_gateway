package com.grd.cpay.payment.alipay.vo.common;

import java.math.BigDecimal;

public class TradeFundBill {

    private String fund_channel;
    private BigDecimal amount;
    private BigDecimal real_amount;

    public String getFund_channel() {
        return fund_channel;
    }

    public void setFund_channel(String fund_channel) {
        this.fund_channel = fund_channel;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getReal_amount() {
        return real_amount;
    }

    public void setReal_amount(BigDecimal real_amount) {
        this.real_amount = real_amount;
    }
}
