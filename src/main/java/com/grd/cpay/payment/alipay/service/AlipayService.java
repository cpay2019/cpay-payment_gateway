package com.grd.cpay.payment.alipay.service;

import com.alipay.api.AlipayApiException;
import com.alipay.api.response.*;
import com.grd.cpay.payment.alipay.vo.req.*;

public interface AlipayService {

    AlipayTradePrecreateResponse precreate(PrecreateReq precreateReq, String appId, String privateKey, String notifyUrl) throws AlipayApiException;

    AlipayTradeCancelResponse cancel(AliCancelReq cancelReq, String appId, String privateKey) throws AlipayApiException;

    AlipayTradeQueryResponse query(AliQueryReq queryReq, String appId, String privateKey) throws AlipayApiException;

    AlipayTradeCloseResponse close(AliCloseReq closeReq, String appId, String privateKey) throws AlipayApiException;

    AlipayDataDataserviceBillDownloadurlQueryResponse downloadbill(AliDownloadBillReq downloadBillReq, String appId, String privateKey) throws AlipayApiException;
}
