package com.grd.cpay.payment.alipay.vo.common;

import com.alipay.api.internal.mapping.ApiField;

import java.math.BigDecimal;

public class SettleDetailInfos {

    private String trans_in_type;
    private String trans_in;
    private String summary_dimension;
    private String settle_entity_id;
    private String settle_entity_type;
    private BigDecimal amount;
    private String merchant_type;

    public String getTrans_in_type() {
        return trans_in_type;
    }

    public void setTrans_in_type(String trans_in_type) {
        this.trans_in_type = trans_in_type;
    }

    public String getTrans_in() {
        return trans_in;
    }

    public void setTrans_in(String trans_in) {
        this.trans_in = trans_in;
    }

    public String getSummary_dimension() {
        return summary_dimension;
    }

    public void setSummary_dimension(String summary_dimension) {
        this.summary_dimension = summary_dimension;
    }

    public String getSettle_entity_id() {
        return settle_entity_id;
    }

    public void setSettle_entity_id(String settle_entity_id) {
        this.settle_entity_id = settle_entity_id;
    }

    public String getSettle_entity_type() {
        return settle_entity_type;
    }

    public void setSettle_entity_type(String settle_entity_type) {
        this.settle_entity_type = settle_entity_type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getMerchant_type() {
        return merchant_type;
    }

    public void setMerchant_type(String merchant_type) {
        this.merchant_type = merchant_type;
    }
}
