package com.grd.cpay.payment.alipay.vo.common;

public class LogisticsDetail {

    private String logistics_type;

    public String getLogistics_type() {
        return logistics_type;
    }

    public void setLogistics_type(String logistics_type) {
        this.logistics_type = logistics_type;
    }
}
