package com.grd.cpay.payment.alipay.vo.common;

public class ExtendParams {

    private String sys_service_provider_id;
    private String industry_reflux_info;
    private String card_type;

    public String getSys_service_provider_id() {
        return sys_service_provider_id;
    }

    public void setSys_service_provider_id(String sys_service_provider_id) {
        this.sys_service_provider_id = sys_service_provider_id;
    }

    public String getIndustry_reflux_info() {
        return industry_reflux_info;
    }

    public void setIndustry_reflux_info(String industry_reflux_info) {
        this.industry_reflux_info = industry_reflux_info;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }
}
