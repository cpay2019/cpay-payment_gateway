package com.grd.cpay.payment.alipay.vo.req;

import com.grd.cpay.payment.alipay.vo.common.ExtendParams;
import com.grd.cpay.payment.alipay.vo.common.GoodsDetail;
import com.grd.cpay.payment.alipay.vo.common.SettleInfo;

import java.math.BigDecimal;

public class PrecreateReq {

    private String out_trade_no;
    private String seller_id;
    private BigDecimal total_amount;
    private BigDecimal discountable_amount;
    private String subject;
    private GoodsDetail goods_detail;
    private String body;
    private String operator_id;
    private String store_id;
    private String disable_pay_channels;
    private String enable_pay_channels;
    private String terminal_id;
    private ExtendParams extend_params;
    private String timeout_express;
    private SettleInfo settle_info;
    private String merchant_order_no;
    private String business_params;
    private String qr_code_timeout_express;

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public BigDecimal getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(BigDecimal total_amount) {
        this.total_amount = total_amount;
    }

    public BigDecimal getDiscountable_amount() {
        return discountable_amount;
    }

    public void setDiscountable_amount(BigDecimal discountable_amount) {
        this.discountable_amount = discountable_amount;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public GoodsDetail getGoods_detail() {
        return goods_detail;
    }

    public void setGoods_detail(GoodsDetail goods_detail) {
        this.goods_detail = goods_detail;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getDisable_pay_channels() {
        return disable_pay_channels;
    }

    public void setDisable_pay_channels(String disable_pay_channels) {
        this.disable_pay_channels = disable_pay_channels;
    }

    public String getEnable_pay_channels() {
        return enable_pay_channels;
    }

    public void setEnable_pay_channels(String enable_pay_channels) {
        this.enable_pay_channels = enable_pay_channels;
    }

    public String getTerminal_id() {
        return terminal_id;
    }

    public void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    public ExtendParams getExtend_params() {
        return extend_params;
    }

    public void setExtend_params(ExtendParams extend_params) {
        this.extend_params = extend_params;
    }

    public String getTimeout_express() {
        return timeout_express;
    }

    public void setTimeout_express(String timeout_express) {
        this.timeout_express = timeout_express;
    }

    public SettleInfo getSettle_info() {
        return settle_info;
    }

    public void setSettle_info(SettleInfo settle_info) {
        this.settle_info = settle_info;
    }

    public String getMerchant_order_no() {
        return merchant_order_no;
    }

    public void setMerchant_order_no(String merchant_order_no) {
        this.merchant_order_no = merchant_order_no;
    }

    public String getBusiness_params() {
        return business_params;
    }

    public void setBusiness_params(String business_params) {
        this.business_params = business_params;
    }

    public String getQr_code_timeout_express() {
        return qr_code_timeout_express;
    }

    public void setQr_code_timeout_express(String qr_code_timeout_express) {
        this.qr_code_timeout_express = qr_code_timeout_express;
    }
}
