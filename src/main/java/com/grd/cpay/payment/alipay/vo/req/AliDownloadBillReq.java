package com.grd.cpay.payment.alipay.vo.req;

public class AliDownloadBillReq {

    private String bill_type;
    private String bill_date;

    public String getBill_type() {
        return bill_type;
    }

    public void setBill_type(String bill_type) {
        this.bill_type = bill_type;
    }

    public String getBill_date() {
        return bill_date;
    }

    public void setBill_date(String bill_date) {
        this.bill_date = bill_date;
    }
}
