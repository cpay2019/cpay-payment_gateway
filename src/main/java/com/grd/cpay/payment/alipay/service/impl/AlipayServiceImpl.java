package com.grd.cpay.payment.alipay.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.request.*;
import com.alipay.api.response.*;
import com.grd.cpay.base.utils.JsonUtils;
import com.grd.cpay.payment.alipay.service.AlipayBaseHttpService;
import com.grd.cpay.payment.alipay.service.AlipayService;
import com.grd.cpay.payment.alipay.vo.req.*;
import org.springframework.stereotype.Service;

@Service
public class AlipayServiceImpl extends AlipayBaseHttpService implements AlipayService {

    private static final String TAG = "AlipayServiceImpl";

    @Override
    public AlipayTradePrecreateResponse precreate(PrecreateReq precreateReq, String appId, String privateKey, String notifyUrl) throws AlipayApiException {
        AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
        request.setNotifyUrl(notifyUrl);
        request.setBizContent(JsonUtils.fromObject(precreateReq));
        return callAlipayAPI(appId, privateKey, request);
    }

    @Override
    public AlipayTradeCancelResponse cancel(AliCancelReq cancelReq, String appId, String privateKey) throws AlipayApiException {
        AlipayTradeCancelRequest request = new AlipayTradeCancelRequest();
        request.setBizContent(JsonUtils.fromObject(cancelReq));
        return callAlipayAPI(appId, privateKey, request);
    }

    @Override
    public AlipayTradeQueryResponse query(AliQueryReq queryReq, String appId, String privateKey) throws AlipayApiException {
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        request.setBizContent(JsonUtils.fromObject(queryReq));
        return callAlipayAPI(appId, privateKey, request);
    }

    @Override
    public AlipayTradeCloseResponse close(AliCloseReq closeReq, String appId, String privateKey) throws AlipayApiException {
        AlipayTradeCloseRequest request = new AlipayTradeCloseRequest();
        request.setBizContent(JsonUtils.fromObject(closeReq));
        return callAlipayAPI(appId, privateKey, request);
    }

    @Override
    public AlipayDataDataserviceBillDownloadurlQueryResponse downloadbill(AliDownloadBillReq downloadBillReq, String appId, String privateKey) throws AlipayApiException {
        AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
        request.setBizContent(JsonUtils.fromObject(downloadBillReq));
        return callAlipayAPI(appId, privateKey, request);
    }
}
