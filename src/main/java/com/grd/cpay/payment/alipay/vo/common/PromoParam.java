package com.grd.cpay.payment.alipay.vo.common;

public class PromoParam {

    private String actual_order_time;

    public String getActual_order_time() {
        return actual_order_time;
    }

    public void setActual_order_time(String actual_order_time) {
        this.actual_order_time = actual_order_time;
    }
}
