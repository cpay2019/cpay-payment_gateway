package com.grd.cpay.payment.alipay.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.grd.cpay.base.vo.BaseResponse;
import com.grd.cpay.payment.alipay.vo.req.AliOrderinfoReq;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/alipay/callback")
@Api(tags = "Alipay Apis")
@ApiIgnore
public class AlipayController {

    @RequestMapping(value = "/orderinfo", method = {RequestMethod.POST})
    @ApiOperation("訂單訊息更新")
    public @ResponseBody
    BaseResponse orderinfo(AliOrderinfoReq aliOrderinfoReq) {
        return new BaseResponse();
    }
}
