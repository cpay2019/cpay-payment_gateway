package com.grd.cpay.payment.alipay.service;

import com.alipay.api.*;
import com.grd.cpay.base.utils.LogUtils;
import org.springframework.beans.factory.annotation.Value;

public class AlipayBaseHttpService {

    private static final String TAG = "AlipayBaseHttpService";

    @Value("${alipay.url}")
    private String ALIPAY_URL;

    private static final String FORMAT = "json";
    private static final String CHARSET = "utf-8";

    protected  <T extends AlipayResponse> T callAlipayAPI(String appId, String privateKey, AlipayRequest<T> request) throws AlipayApiException {
        AlipayClient alipayClient = new DefaultAlipayClient(ALIPAY_URL,appId,privateKey,FORMAT,CHARSET);
        T response = alipayClient.execute(request);
        if(response.isSuccess()) {
            return response;
        } else {
            LogUtils.error(TAG, "调用失败: " + response.getMsg());
            return null;
        }
    }
}
