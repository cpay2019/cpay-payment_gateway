package com.grd.cpay.payment.alipay.vo.common;

public class SettleInfo {

    private SettleDetailInfos settle_detail_infos;
    private String merchant_type;

    public SettleDetailInfos getSettle_detail_infos() {
        return settle_detail_infos;
    }

    public void setSettle_detail_infos(SettleDetailInfos settle_detail_infos) {
        this.settle_detail_infos = settle_detail_infos;
    }

    public String getMerchant_type() {
        return merchant_type;
    }

    public void setMerchant_type(String merchant_type) {
        this.merchant_type = merchant_type;
    }
}
