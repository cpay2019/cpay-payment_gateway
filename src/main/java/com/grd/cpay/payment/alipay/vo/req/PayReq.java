package com.grd.cpay.payment.alipay.vo.req;

import com.grd.cpay.payment.alipay.vo.common.ExtendParams;
import com.grd.cpay.payment.alipay.vo.common.GoodsDetail;
import com.grd.cpay.payment.alipay.vo.common.PromoParam;

import java.math.BigDecimal;

public class PayReq {

    private String out_trade_no;
    private String scene;
    private String auth_code;
    private String product_code;
    private String subject;
    private String buyer_id;
    private String seller_id;
    private BigDecimal total_amount;
    private String trans_currency;
    private String settle_currency;
    private BigDecimal discountable_amount;
    private String body;
    private GoodsDetail goods_detail;
    private String operator_id;
    private String store_id;
    private String terminal_id;
    private ExtendParams extend_params;
    private String timeout_express;
    private String auth_confirm_mode;
    private String terminal_params;
    private PromoParam promo_params;

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public BigDecimal getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(BigDecimal total_amount) {
        this.total_amount = total_amount;
    }

    public BigDecimal getDiscountable_amount() {
        return discountable_amount;
    }

    public void setDiscountable_amount(BigDecimal discountable_amount) {
        this.discountable_amount = discountable_amount;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public GoodsDetail getGoods_detail() {
        return goods_detail;
    }

    public void setGoods_detail(GoodsDetail goods_detail) {
        this.goods_detail = goods_detail;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getTerminal_id() {
        return terminal_id;
    }

    public void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    public ExtendParams getExtend_params() {
        return extend_params;
    }

    public void setExtend_params(ExtendParams extend_params) {
        this.extend_params = extend_params;
    }

    public String getTimeout_express() {
        return timeout_express;
    }

    public void setTimeout_express(String timeout_express) {
        this.timeout_express = timeout_express;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getAuth_code() {
        return auth_code;
    }

    public void setAuth_code(String auth_code) {
        this.auth_code = auth_code;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getTrans_currency() {
        return trans_currency;
    }

    public void setTrans_currency(String trans_currency) {
        this.trans_currency = trans_currency;
    }

    public String getSettle_currency() {
        return settle_currency;
    }

    public void setSettle_currency(String settle_currency) {
        this.settle_currency = settle_currency;
    }

    public String getAuth_confirm_mode() {
        return auth_confirm_mode;
    }

    public void setAuth_confirm_mode(String auth_confirm_mode) {
        this.auth_confirm_mode = auth_confirm_mode;
    }

    public String getTerminal_params() {
        return terminal_params;
    }

    public void setTerminal_params(String terminal_params) {
        this.terminal_params = terminal_params;
    }

    public PromoParam getPromo_params() {
        return promo_params;
    }

    public void setPromo_params(PromoParam promo_params) {
        this.promo_params = promo_params;
    }
}
