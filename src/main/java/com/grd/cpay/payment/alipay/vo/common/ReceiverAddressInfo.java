package com.grd.cpay.payment.alipay.vo.common;

public class ReceiverAddressInfo {

    private String name;
    private String address;
    private String mobile;
    private String zip;
    private String divisioin_code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getDivisioin_code() {
        return divisioin_code;
    }

    public void setDivisioin_code(String divisioin_code) {
        this.divisioin_code = divisioin_code;
    }
}
