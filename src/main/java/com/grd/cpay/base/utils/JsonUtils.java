package com.grd.cpay.base.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class JsonUtils {
	protected static Logger logger = LoggerFactory.getLogger(JsonUtils.class);

	private static ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.enable(SerializationFeature.INDENT_OUTPUT);  //換行
	}

	public static <T> T fromJson(String json, Class<T> clazz) {
		T obj = null;
		try {
			obj = mapper.readValue(json, clazz);
		} catch (JsonParseException e) {
			logger.error(e.getMessage(), e);
		} catch (JsonMappingException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return obj;
	}

	public static <T> T fromJson(String json, TypeReference<T> typeReference) {
		T obj = null;
		try {
			obj = mapper.readValue(json, typeReference);
		} catch (JsonParseException e) {
			logger.error(e.getMessage(), e);
		} catch (JsonMappingException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return obj;
	}
	
	public static String fromObject(Object obj) {
		String s = "";
		try {
			s = mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage(), e);
		}
		return s;
	}
	
	
	public static <T, T1> Object responseFromJson(String json, Class<T> clazz, @SuppressWarnings("rawtypes") Class<T1> clazzWrapper) {
		JavaType type = mapper.getTypeFactory().constructParametricType(clazzWrapper, clazz);
		T1 t1 = null;
		try {
			t1 = mapper.readValue(json, type);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
		}
		return t1;
	}

	public static <T, T1> Object responseFromJsonList(String json, Class<T> clazz, @SuppressWarnings("rawtypes") Class<T1> clazzWrapper) {
		JavaType type = mapper.getTypeFactory().constructParametricType(List.class, clazz);
		type = mapper.getTypeFactory().constructParametricType(clazzWrapper, type);
		T1 t1 = null;
		try {
			t1 = mapper.readValue(json, type);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage(), e);
		}
		return t1;
	}
	
}
