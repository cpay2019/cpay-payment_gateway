package com.grd.cpay.base.vo;

public class BaseResponse<T> {

    private String message;
    private String code;
    private T data;

    public BaseResponse() {
        this.message = BaseResStatus.SUCCESS.getMessage();
        this.code=BaseResStatus.SUCCESS.getCode();
    }

    public BaseResponse(T t) {
        this.message = BaseResStatus.SUCCESS.getMessage();
        this.code=BaseResStatus.SUCCESS.getCode();
        this.data = t;
    }

    public BaseResponse(String message) {
        this.code=BaseResStatus.FAIL.getCode();
        this.message = message;
    }

    public BaseResponse(BaseResStatus s) {
        this.code = s.getCode();
        this.message = s.getMessage();
    }

    public BaseResponse(BaseResStatus s, String message) {
        this.code = s.getCode();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
