package com.grd.cpay.base.vo;

public enum BaseResStatus {

    SUCCESS("SUCCESS", "OK"),
    FAIL("FAIL", "FAIL");

    private String code;
    private String message;

    BaseResStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
