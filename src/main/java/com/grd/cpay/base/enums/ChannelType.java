package com.grd.cpay.base.enums;

public enum ChannelType {
    ALI, WECHAT, UNION;
}
