package com.grd.cpay.base.controller;

public abstract class BaseController {

	/**
	 * Simple class name
	 * 
	 * @return
	 */
	public String getTag() {
		return getClass().getName();
	}

}