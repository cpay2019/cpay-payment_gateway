package com.grd.cpay.base.service;

import com.grd.cpay.base.utils.JsonUtils;
import com.grd.cpay.base.utils.LogUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.nio.charset.Charset;

public class BaseHttpService {

    private static final String TAG = "BaseHttpService";

    /**
     *
     * @param url
     * @param httpMethod
     * @param bodyStr
     * @return
     */
    protected String sendHttpRequest(String url, HttpMethod httpMethod, String bodyStr) {

        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
            RequestEntity requestEntity = new RequestEntity(bodyStr, httpMethod, new URI(url));
            LogUtils.info(TAG, String.format("Request Url:%s, Method:%s, Body:%s", requestEntity.getUrl(), requestEntity.getMethod(), bodyStr));
            HttpEntity<String> response = restTemplate.exchange(requestEntity, String.class);
            LogUtils.info(TAG, "Response:"+ JsonUtils.fromObject(response.getBody()));
            return response.getBody();
        } catch (Exception e) {
            LogUtils.error(TAG, e);
            LogUtils.error(TAG, "url: " +url);
            return null;
        }

    }
}
