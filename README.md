# Combo Pay

## 1. Introduction
- Combo Pay是一個聚合支付，整合支付寶、雲閃付...等相關第三支付。  
- 提供快速便捷的支付服務，快速提供商家進行支付串接與多帳戶整合。

## 2. Get started
### 2.1 Certification
- **Certification Folder** [resource/cert](https://gitlab.com/cpay2019/cpay-payment_gateway/tree/master/src/main/resources/cert)
- **API Infomaction** [application.yml](https://gitlab.com/cpay2019/cpay-payment_gateway/blob/master/src/main/resources/application.yml)
```
wechat:
  url: https://api.mch.weixin.qq.com

union:
  url: https://gateway.test.95516.com
  sign-cert: /cert/acp_test_sign.pfx
  middle-cert: /cert/acp_test_middle.cer
  root-cert: /cert/acp_test_root.cer
  cert-pwd: '000000'
  notifyUrl: https://webhook.site/0f71a1f8-4f7a-4672-a8b9-383977847160

alipay:
  url: https://openapi.alipaydev.com/gateway.do
  notifyUrl: https://webhook.site/0f71a1f8-4f7a-4672-a8b9-383977847160
```

### 2.2 API Doc
- **Swagger** http://localhost:8090/swagger-ui.html
![](https://gitlab.com/cpay2019/cpay-payment_gateway/raw/master/src/main/resources/static/images/swagger-01.jpg)