FROM openjdk:8-jre-alpine
VOLUME /tmp
ADD target/payment-gateway-1.0-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE 6565
ENV JAVA_OPTS=""
ENV SPRING_PROFILES_ACTIVE=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/urandom -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE -jar /app.jar" ]